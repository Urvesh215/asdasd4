<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
       <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
                        <li class="open"><a class="sidebar-sub-toggle"><i class="ti-user"></i> Admin <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="profile.php">PROFILE</a></li>
                                <li><a href="company.php">COMPANY</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="expenses.php">EXPENSES</a></li>
                                <li><a href="../stakeholder/stakeholder_list.php">STAKEHOLDER</a></li>
                                <li><a href="../group/group_list.php">GROUP</a></li>
                                <li><a href="../agent/agent_list.php">AGENT</a></li>
                                <li><a href="../player/player_list.php">PLAYER</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="../intake/intake_acc_list.php">INTAKE account</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="../outset/outset_list.php">OUTSET</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="../reports/full_report.php">RESULT</a></li>
                                <li><a href="../common/payment.php">PAYMENT</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="../common/payment_history.php">payment HISTORY</a></li>
                                <li><a href="change_password.php">PASSWORD</a></li>
                                
                                
                                
                            </ul>
                        </li>

                        <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> Bets <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="../Bets/place_bets.php">PLACE BETS</a></li>
                                <li><a href="../Bets/uploud_bets.php">Upload bets</a></li>
                                <li ><a href="../Bets/fixed_bets.php">Fixed bets</a></li>
                                <li><a href="../Bets/Out_Bets.php">out bets</a></li>
                                <li><a href="../Bets/Process_outset.php">process outset</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="../Bets/summary_bets.php">summary bets</a></li>
                                <li><a href="../Bets/View_tickets.php">view tickets</a></li>
                                <li><a href="../Bets/Search_tickets.php">search tickets</a></li>
                                <li><a href="../Bets/History_tickets.php">history</a></li>
                                
                                
                                
                            </ul>
                        </li>

                        <li><a class="sidebar-sub-toggle"><i class="ti-receipt"></i>Reports <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="../reports/full_report.php">full report</a></li>
                                <li><a href="../reports/Group_report.php">group report</a></li>
                                <li><a href="../reports/Stake_report.php">stake report</a></li>
                                <li><a href="../reports/Strike_report.php">strike report</a></li>
                                <li><a href="../reports/Summary_report.php">summary report</a></li>
                                
                                
                                
                            </ul>
                        </li>

                        <li><a class="sidebar-sub-toggle"><i class="ti-help-alt"></i> Options <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li style="border-bottom: 1px solid black;"><a href="../options/Message.php">message</a></li>
                                <li style="border-bottom: 1px solid black;"><a href="../options/announcement.php">announcment</a></li>
                                <li><a href="../options/Regulations.php">regulations</a></li>
                                
                                
                                
                            </ul>
                        </li>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.bwyn138.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>

<div id="main">
<div>
        <div class="col-md-12">
            <h2>Company Managment</h2>
            <button style="float: right;" class="btn3">Change Password</button>
            <br>
        </div>
    </div>

    <div>
        <div class="col-md-8">
            <table class="table">
                <thead>
                <tr>
                    <th colspan="2">Account Details</th>
                </tr>
                </thead>
                <tbody>
                <tr class="td_60">
                    <td>User ID</td><td><?php echo $results['user_id']; ?></td>
                </tr>
                <tr>
                    <td>Name</td><td><?php echo $results['user_id']; ?></td>
                </tr>
                <tr>
                    <td>Credit Limit</td><td>SGD <?php echo $results['credit_limit']; ?></td>
                </tr>
                <tr>
                    <td>Account Balance</td><td>Not Implemented</td>
                </tr>
                <tr>
                    <td>Credit Alocated</td><td>Not Implemented</td>
                </tr>
                <tr>
                    <td>Credit Left</td><td>Not Implemented</td>
                </tr>
                </tbody>
            </table>

            <table class="table">
                <thead>
                <tr>
                    <th colspan="2">4D Settings</th>
                </tr>
                </thead>
                <tbody>
                <tr class="td_60">
                    <td>Big Rate</td><td>SGD <?php echo $results['big_rate']; ?></td>
                </tr>
                <tr>
                    <td>Small Rate</td><td>SGD <?php echo $results['small_rate']; ?></td>
                </tr>
                <tr>
                    <td>Big Ticket Intake</td><td><?php echo $results['big_ticket_intake']; ?></td>
                </tr>
                <tr>
                    <td>Small Ticket Intake</td><td><?php echo $results['small_ticket_intake']; ?></td>
                </tr>
                <tr>
                    <td>Intake Method</td><td><?php if($results['big_priority']) { echo 'Big Priority'; } if($results['in_big_out_small']) { echo 'In Big Out Small';} if($results['all_big_and_small']) { echo 'All Big And Small'; } ?></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td><button class="btn3">View Steak Alocation</buttom></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>