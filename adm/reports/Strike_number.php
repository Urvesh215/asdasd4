<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div id="clock"></div>
            <p id="date"></p>
            <div class="divbtn"><button class="btn1" ">Logout</button></div>
            <div class="logo"><a href="#">LOGO</a></div>
            <ul>
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i> Admin <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../admin/profile.php">PROFILE</a></li>
                        <li><a href="../admin/company.php">COMPANY</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../admin/expenses.php">EXPENSES</a></li>
                        <li><a href="../stakeholder/stakeholder_list.php">STAKEHOLDER</a></li>
                        <li><a href="../group/group_list.php">GROUP</a></li>
                        <li><a href="../agent/agent_list.php">AGENT</a></li>
                        <li><a href="../player/player_list.php">PLAYER</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../intake/intake_acc_list.php">INTAKE account</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../outset/outset_list.php">OUTSET</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../reports/full_report.php">RESULT</a></li>
                        <li><a href="../common/payment.php">PAYMENT</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../common/payment_history.php">payment HISTORY</a></li>
                        <li><a href="change_password.php">PASSWORD</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> Bets <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../Bets/place_bets.php">PLACE BETS</a></li>
                        <li><a href="../Bets/uploud_bets.php">Upload bets</a></li>
                        <li ><a href="../Bets/fixed_bets.php">Fixed bets</a></li>
                        <li><a href="../Bets/Out_Bets.php">out bets</a></li>
                        <li><a href="../Bets/Process_outset.php">process outset</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../Bets/summary_bets.php">summary bets</a></li>
                        <li><a href="../Bets/View_tickets.php">view tickets</a></li>
                        <li><a href="../Bets/Search_tickets.php">search tickets</a></li>
                        <li><a href="../Bets/History_tickets.php">history</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-receipt"></i>Reports <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../reports/full_report.php">full report</a></li>
                        <li><a href="../reports/Group_report.php">group report</a></li>
                        <li><a href="../reports/Stake_report.php">stake report</a></li>
                        <li><a href="../reports/Strike_report.php">strike report</a></li>
                        <li><a href="../reports/Summary_report.php">summary report</a></li>



                    </ul>
                </li>

                <li class="open"><a class="sidebar-sub-toggle"><i class="ti-help-alt"></i> Options <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li style="border-bottom: 1px solid black;"><a href="../options/Message.php">message</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../options/announcement.php">announcment</a></li>
                        <li><a href="../options/Regulations.php">regulations</a></li>



                    </ul>
                </li>


            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.bwyn138.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
    <div class="col-md-5">
        <h3 class="page_title">Strike Report</h3><br>
    </div>
    <div class="col-md-8 responsive_side_align">
        <form id="search-form" onclick="return false" action="" class="form-inline" style="display: inline;">
            <label style="display: inline;">User ID/Name</label>
            <input type="text" class="form-control form-control-sm btn-sm filter" id="search-filter">
            <button id="find" type="submit" class="btn3">Find</button>
        </form>

        <button id="clear" class="btn3" style="display: inline;">All</button>
        <select style="width: 150px;" name="cars">
          <option value="Account">Account</option>
          <option value="Account">Account</option>
          <option value="Account">Account</option>
          <option value="Account">Account</option>
        </select>
        <label style="display: inline;">From</label>

          <input type="date"><Date</input>

        </select>
         <label style="display: inline;">To</label>

          <input type="date"><Date</input>

        </select>
        <button class="btn1">View</button>
    </div>
</div><br>


<div class="col-md-12">
<h3>/..Account name../ Account For /..Date../ View by Number</h3>
<button style="float: right;" class="btn1">Print</button>
<button style="float: right;" class="btn1">View by Account</button>
<button style="float: right;" class="btn1">View Summary</button><br>
</div>
<br>

<div class="col-md-12">
    <div class="col-md-12 pl-0">
    <table id="stakeholders" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
            <tr style="background-color: #728b85;">
                <th>P</th>
                <th>4D</th>
                <th>Big</th>
                <th>Big i</th>
                <th>Small</th>
                <th>Small i</th>
                <th>Strike $</th>
                <th>Account</th>
                <th>Origin</th>
                <th>Time</th>
                <th>Page</th>
                <th>#</th>
            </tr>
            <tr>
                    <td>1</td>
                    <td>23</td>
                    <td>24</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
            </tr>
            <tr>
                    <td></td>
                    <td>Total</td>
                    <td>24</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td>123</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
            </tr>
    </table>
    </div>
</div>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>