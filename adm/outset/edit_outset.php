<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <div id="clock"></div>
            <p id="date"></p>
            <div class="divbtn"><button class="btn1" ">Logout</button></div>
            <div class="logo"><a href="#">LOGO</a></div>
            <ul>
                <li class="open"><a class="sidebar-sub-toggle"><i class="ti-user"></i> Admin <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../admin/profile.php">PROFILE</a></li>
                        <li><a href="../admin/company.php">COMPANY</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../admin/expenses.php">EXPENSES</a></li>
                        <li><a href="../stakeholder/stakeholder_list.php">STAKEHOLDER</a></li>
                        <li><a href="../group/group_list.php">GROUP</a></li>
                        <li><a href="../agent/agent_list.php">AGENT</a></li>
                        <li><a href="../player/player_list.php">PLAYER</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../intake/intake_acc_list.php">INTAKE account</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../outset/outset_list.php">OUTSET</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../reports/full_report.php">RESULT</a></li>
                        <li><a href="../common/payment.php">PAYMENT</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../common/payment_history.php">payment HISTORY</a></li>
                        <li><a href="change_password.php">PASSWORD</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-money"></i> Bets <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../Bets/place_bets.php">PLACE BETS</a></li>
                        <li><a href="../Bets/uploud_bets.php">Upload bets</a></li>
                        <li ><a href="../Bets/fixed_bets.php">Fixed bets</a></li>
                        <li><a href="../Bets/Out_Bets.php">out bets</a></li>
                        <li><a href="../Bets/Process_outset.php">process outset</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../Bets/summary_bets.php">summary bets</a></li>
                        <li><a href="../Bets/View_tickets.php">view tickets</a></li>
                        <li><a href="../Bets/Search_tickets.php">search tickets</a></li>
                        <li><a href="../Bets/History_tickets.php">history</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-receipt"></i>Reports <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li><a href="../reports/full_report.php">full report</a></li>
                        <li><a href="../reports/Group_report.php">group report</a></li>
                        <li><a href="../reports/Stake_report.php">stake report</a></li>
                        <li><a href="../reports/Strike_report.php">strike report</a></li>
                        <li><a href="../reports/Summary_report.php">summary report</a></li>



                    </ul>
                </li>

                <li><a class="sidebar-sub-toggle"><i class="ti-help-alt"></i> Options <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        <li style="border-bottom: 1px solid black;"><a href="../options/Message.php">message</a></li>
                        <li style="border-bottom: 1px solid black;"><a href="../options/announcement.php">announcment</a></li>
                        <li><a href="../options/Regulations.php">regulations</a></li>



                    </ul>
                </li>


            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.bwyn138.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
<div class="col-md-12">
    <h2>Outset Managment</h2><br>
</div>
</div>
<div>
    <div class="col-md-12">
        <h3 style="margin-bottom: 20px;">Add Outset</h3><br>
    </div>
</div>

    <form method="POST"1>
    <div>
        <div class="col-md-5">
            <div class="form-group">
                <label>User ID</label>
                <input type="text" name="username" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control form-control-sm" id="password">
            </div>
            <div class="form-group">
                <label>Credit Limit</label>
                <input type="number" name="credit_limit" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Outset Priority</label>
                <input type="number" name="priority" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Status</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" checked value="Active">Active</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" value="Lock">Lock</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" value="Terminate">Terminate</label>
                </div>
            </div>
        </div>

        <div class="col-md-7">
            <div class="form-group">
                <label>4D Settings</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="d4_settings" class="form-check-input" value="0">No</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="d4_settings" class="form-check-input" value="1">Yes</label>
                </div>
            </div>
            <div class="form-group">
                <label>Big Rate</label>
                <input type="number" class="form-control form-control-sm" readonly="true" value="1.60">
            </div>
            <div class="form-group">
                <label>Small Rate</label>
                <input type="number" class="form-control form-control-sm" readonly="true" value="0.70">
            </div>
            <div class="form-group">
                <label>Ticket Rebate</label>
                <input type="number" step="any" name="rebate" class="form-control form-control-sm">
                <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
            </div>
            <div class="form-group">
                <label>Strike_commission</label>
                <input type="number" step="any" name="strike_commission" class="form-control form-control-sm">
                <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
            </div>
            <div class="form-group">
                <label>Big Ticket Intake</label>
                <input type="number" name="big_ticket_intake" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Small Ticket Intake</label>
                <input type="number" name="small_ticket_intake" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                    <label>Intake Method</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" checked value="All Big And Small">All Big And Small
                        </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" value="Big Priority">Big Priority</label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" value="Small Priority">Small Priority</label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" value="Big Only">Big Only</label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" value="Small Only">Small Only</label>
                    </div>
                    
                </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn2">Save</button>
        </div>
    </div>
    </form>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>