<?php 

session_start();
if($_SESSION['logged_in'] == 'true')
{

    if($_SESSION['acct_level'] == 'admin') {
        header("Location:adm/admin/admin.php");
        echo "<script type='text/javascript'>alert('admin!');</script>";
        ##echo "<script type='text/javascript'> document.location = 'adm/admin/admin.php'; </script>";

    }

    elseif($_SESSION['acct_level'] == 'agent') {
        header("Location:agent/options/Regulations.php");
        echo "<script type='text/javascript'>alert('agent!');</script>";
        ##echo "<script type='text/javascript'> document.location = 'agent/options/Regulations.php'; </script>";

    }
    elseif($_SESSION['acct_level'] == 'player') {
        header("Location:player/options/Regulations.php");
        echo "<script type='text/javascript'>alert('player!');</script>";
        ##echo "<script type='text/javascript'> document.location = 'player/options/Regulations.php'; </script>";

    }
    elseif($_SESSION['acct_level'] == 'stakeholder') {
        header("Location:stakeholder/options/Regulations.php");
        echo "<script type='text/javascript'>alert('stakeholder!');</script>";
        ##echo "<script type='text/javascript'> document.location = 'stakeholder/options/Regulations.php'; </script>";
        
    }

}
else header("Location:login.php");
exit;
?>