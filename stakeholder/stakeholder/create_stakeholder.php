<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
              <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
                        <?php require_once '../_sidebar/sidebar.php'; ?>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.vegas128.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
 <div class="col-md-12">
    <h2>Stakeholders Managment</h2><br>
</div>
</div>
   
<div class="col-md-12">
    <h3>Add Stakeholder</h3><br>
</div>

    <form class="mb-3" method="POST">

    <div>
        <div class="col-md-5">
                <div class="form-group">
                    <label>User ID</label>
                    <input type="text" name="username" class="form-control form-control-sm">
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control form-control-sm">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control form-control-sm" name="password">
                </div>
                <div class="form-group">
                    <label>Credit Limit</label>
                    <input type="number" name="credit_limit" class="form-control form-control-sm" value="{{ old('credit_limit') }}">
                </div>
                <div class="form-group">
                    <label>Create Intake</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                            <input type="radio" name="create_intake" class="form-check-input" checked value="0"> No
                        </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                            <input type="radio" name="create_intake" class="form-check-input" {{old('create_intake') == '1'?'checked':''}} value="1"> Yes
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Create Agent</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" name="create_agent" checked value="0">No
                        </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input" {{old('create_agent') == '1'?'checked':''}} name="create_agent" value="1">Yes
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Place Bets</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="place_bets" class="form-check-input" checked value="0">No
                    </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="place_bets" {{old('place_bets') == '1'?'checked':''}} class="form-check-input" value="1">Yes
                    </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="status" class="form-check-input" checked value="Active">Active
                    </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="status" class="form-check-input" {{old('status') == 'Lock'?'checked':''}}  value="Lock">Lock
                    </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="status" {{old('status') == 'Terminate'?'checked':''}} class="form-check-input" value="Terminate">Terminate
                    </label>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div class="form-group">
                    <label>4D Settings</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="d4_settings" {{old('d4_settings') == '0'?'checked':''}} class="form-check-input" value="0">No
                    </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="d4_settings" class="form-check-input" {{old('d4_settings') != '0'?'checked':''}} value="1">Yes
                    </label>
                    </div>
                </div>
                <div class="form-group">
                    <label>Big Rate</label>
                    <input type="text" readonly class="form-control form-control-sm" value="1.60" />
                </div>
                <div class="form-group">
                    <label>Small Rate</label>
                    <input type="text" readonly class="form-control form-control-sm" value="0.70" />
                </div>
                <div class="form-group">
                    <label>Ticket Rebate</label>
                    <input type="number" step="any" class="form-control form-control-sm" name="rebate" value="{{ old('rebate')}}">
                    <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
                </div>
                <div class="form-group">
                    <label>Strike Commission</label>
                    <input type="number" step="any" class="form-control form-control-sm" name="strike_commission" value="{{ old('strike_commission')}}">
                    <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
                </div>
                <div class="form-group">
                    <label>Big Ticket Intake</label>
                    <input type="number" class="form-control form-control-sm" name="big_ticket_intake" value="{{ old('big_ticket_intake')}}">
                </div>
                <div class="form-group">
                    <label>Small Ticket Intake</label>
                    <input type="number" class="form-control form-control-sm" name="small_ticket_intake" value="{{ old('small_ticket_intake')}}">
                </div>
                <div class="form-group">
                    <label>Intake Method</label>
                    <br>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" checked value="Big Priority">Big Priority
                        </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" class="form-check-input" {{old('intake_method') == 'Small Priority'?'checked':''}} value="Small Priority">Small Priority
                    </label>
                    </div>
                    <div class="form-check form-check-inline ">
                        <label class="form-check-label">
                        <input type="radio" name="intake_method" {{old('intake_method') == 'All Big and Small'?'checked':''}} class="form-check-input" value="All Big and Small">All Big and Small
                    </label>
                    </div>
                </div>
            </div>
    </div>
    <div class="col-md-12">
        <button class="btn3">Save</button>
    </div>
    </form>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>