<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bwyn 138</title>
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Sidebar CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/sidebar.css">
    <!-- Themify-icons CSS -->
    <link href="assets/css/themify-icons.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body onload="startTime()">


<!-- sidebar -->
       <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <div id="clock"></div>
                    <p id="date"></p>
                    <div class="divbtn"><button class="btn1" ">Logout</button></div>
                    <div class="logo"><a href="#">LOGO</a></div>
                    <ul>
                        <?php require_once '../bets/stakeholder_sidebar_primary_section.php'; ?>

                        <?php require_once '../_sidebar/admin_sidebar_bets_section.php'; ?>

                        <?php require_once '../_sidebar/admin_Sidebar_reports_section.php'; ?>

                        <?php require_once '../bets/stakeholder_sidebar_regulation_section.php'; ?>

                        
                    </ul>
                </div>
            </div>
        </div>
<!-- /# sidebar -->



<div id="d1">
<marquee style="color: white; font-size: 17px;" behavior="scroll" direction="left">4D Bet closes at  6:15pm on Draw days  ****** Mobile Access : wap.vegas128.com ******  Fixed Bets will not be allow to edit/delete on draw days after 3:50pm.   Thank you for the support ! ! !  Goodluck ! ! ! </marquee>
</div>


<div id="main">
<div>
<div class="col-md-12">
    <h2>Agent Managment</h2><br>
</div>
</div>
<div>
    <div class="col-md-12">
        <h3 style="margin-bottom: 20px;">Add Agent</h3><br>
    </div>
</div>

    <form method="POST">
    <div>
        <div class="col-md-5">
            <div class="form-group">
                @if($agent_username != '')
                <label>User ID <span class="justify-content-md-right"><b>under: {{$creator->username}}</b></span></label>
                <div class="input-group mb-2">
                    <input type="text" name="username" readonly class="form-control form-control-sm">
                </div>
            </div>
            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control form-control-sm" id="password">
            </div>
            <div class="form-group">
                <label>Credit Limit</label>
                <input type="number" name="credit_limit" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Create Intake</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input type="radio" name="create_intake" class="form-check-input" checked value="0"> No
                    </label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input type="radio" name="create_intake" class="form-check-input">Yes</label>
                </div>
            </div>
            <div class="form-group">
                <label>Create Agent</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" name="create_agent" checked value="0">No</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input">Yes</label>
                </div>
            </div>
            <div class="form-group">
                <label>Place Bets</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="place_bets" class="form-check-input" checked value="0">No</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="place_bets" class="form-check-input">Yes</label>
                </div>
            </div>
            <div class="form-group">
                <label>Status</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input" checked value="Active">Active
                </label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input">Lock</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="status" class="form-check-input">Terminate</label>
                </div>
            </div>

        </div>

        <div class="col-md-7">
            <div class="form-group">
                <label>4D Settings</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="d4_settings" class="form-check-input">No</label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                    <input type="radio" name="d4_settings" class="form-check-input">Yes</label>
                </div>
            </div>
            <div class="form-group">
                <label>Big Rate</label>
                <input type="number" class="form-control form-control-sm" readonly="true" value="1.60">
            </div>
            <div class="form-group">
                <label>Small Rate</label>
                <input type="number" class="form-control form-control-sm" readonly="true" value="0.70">
            </div>
            <div class="form-group">
                <label>Ticket Rebate</label>
                <input type="number" step="any" name="rebate" class="form-control form-control-sm">
                <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
            </div>
            <div class="form-group">
                <label>Strike_commission</label>
                <input type="number" step="any" name="strike_commission" class="form-control form-control-sm">
                <small class="form-text text-muted">% (e.g. 0% to 20%)</small>
            </div>
            <div class="form-group">
                <label>Big Ticket Intake</label>
                <input type="number" name="big_ticket_intake" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Small Ticket Intake</label>
                <input type="number" name="small_ticket_intake" class="form-control form-control-sm">
            </div>
            <div class="form-group">
                <label>Intake method</label>
                <br>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" checked name="intake_method" value="Big Priority">Big Priority
                    </label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="intake_method" value="Small Priority">Small Priority
                    </label>
                </div>
                <div class="form-check form-check-inline ">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="intake_method" value="All Big and Small">All Big and Small
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn2">Save</button>
        </div>
    </div>
    </form>
</div>











    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.nanoscroller.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Sidebar JavaScript -->
    <script type="text/javascript" src="assets/js/sidebar.js"></script>    
    <!-- Costum JS -->
    <script type="text/javascript" src="assets/js/script.js"></script>
</body>

</html>